const burger = document.querySelector(".burger");
const navBar = document.querySelector("nav");
const LiNode = document.querySelectorAll("nav ul li");
const start = document.querySelector(".start");
const getKnow = document.querySelector(".getKnow");
const offer = document.querySelector(".offer");
const contact = document.querySelector(".contact");
const banner = document.querySelector("header h1");
const mainBanner = document.querySelector("main h1");
const offerBanner = document.querySelector("section h1");
const contactBanner = document.querySelector("article h1");

burger.addEventListener("click", menuSlide);


function menuSlide() {
    navBar.classList.toggle("active");
    burger.classList.toggle("toggle");
    LiNode.forEach(li => {
        if (li.style.animation) {
            li.style.animation = "";
        } else {
            li.style.animation = "fade 1s forwards linear "
            // li.style.animationDelay = ".5s"
        }
    })
}
setInterval(photoSwap, 11000);

function photoSwap() {
    const circle1 = document.querySelector(".circle1");
    const circle2 = document.querySelector(".circle2");
    const circle3 = document.querySelector(".circle3");
    const car1 = document.querySelector(".car1");
    const car2 = document.querySelector(".car2");
    const car3 = document.querySelector(".car3");
    setTimeout(function () {
        car3.classList.add("opacity");
        car1.classList.remove("opacity");

        circle3.classList.remove("activePhoto");
        circle1.classList.add("activePhoto");
    }, 1000)
    setTimeout(function () {
        car1.classList.add("opacity");
        car2.classList.remove("opacity");

        circle1.classList.remove("activePhoto");
        circle2.classList.add("activePhoto");
    }, 4000)
    setTimeout(function () {
        car2.classList.add("opacity");
        car3.classList.remove("opacity");

        circle2.classList.remove("activePhoto");
        circle3.classList.add("activePhoto");
    }, 8000)
}

window.addEventListener("scroll", AwesomeAnimation)

function AwesomeAnimation() {
    const firstText = document.querySelector(".first");
    const secondText = document.querySelector(".second");
    const mainH2Opacity = document.querySelector("main h2");
    let height = window.scrollY;
    if (height > 10) {
        firstText.classList.add("slide");
    } else {
        firstText.classList.remove("slide");
    }

    if (height > 200) {
        secondText.classList.add("slide");
    } else {
        secondText.classList.remove("slide");
    }

    if (height > 525) {
        mainH2Opacity.classList.add("noOpacity");
    } else {
        mainH2Opacity.classList.remove("noOpacity");
    }

    if (height > 0) {
        navBar.classList.add("sticky");
        burger.classList.add("sticky");
        banner.classList.add("sticky");
    } else {
        navBar.classList.remove("sticky");
        burger.classList.remove("sticky");
        banner.classList.remove("sticky");
    }
}
function offset(el) {
    var rect = el.getBoundingClientRect(),
    scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    return { top: rect.top + scrollTop}
}
let bannerOffset = offset(banner)
let mainBannerOffset = offset(mainBanner)
console.log(mainBannerOffset);
let offerBannerOffset = offset(offerBanner)
let contactBannerOffset = offset(contactBanner)
console.log(contactBannerOffset)


getKnow.addEventListener("click", getKnowSlide);

function getKnowSlide() {
    let getKnowOption = {
        left: 0,
        top: mainBannerOffset.top,
        behavior: 'smooth'
    }
    window.scrollTo(getKnowOption);
}

start.addEventListener("click", startSlide);

function startSlide() {
    let startOption = {
        left: 0,
        top: bannerOffset.top,
        behavior: 'smooth'
    }
    window.scrollTo(startOption)
}

offer.addEventListener("click", offerSlide);

function offerSlide() {
    let offerOption = {
        left: 0,
        top: offerBannerOffset.top,
        behavior: 'smooth'
    }
    window.scrollTo(offerOption)
}
contact.addEventListener("click", contactSlide);

function contactSlide() {
    let contactOption = {
        left: 0,
        top: contactBannerOffset.top,
        behavior: 'smooth'
    }
    window.scrollTo(contactOption);
}